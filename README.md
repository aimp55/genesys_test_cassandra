# Evaluating Apache Cassandra

This project aims to evaluate Cassandra as a potential replacement of RDMBS
for PGR data management in [Genesys](https://www.genesys-pgr.org/).

## A quick start

First, download and install [Cassandra 2.1.3 or greater](https://cassandra.apache.org/download/)
on your local machine or change `cassandra.properties` to connect to your Cassandra instance.

Create `genesystest` keyspace on Cassandra:

		cqlsh> CREATE KEYSPACE genesystest WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };`)

Run the tests.

