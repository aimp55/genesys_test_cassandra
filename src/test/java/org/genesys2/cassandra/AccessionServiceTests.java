/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.cassandra;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.genesys2.cassandra.entity.Accession;
import org.junit.Test;

import com.datastax.driver.core.utils.UUIDs;

public class AccessionServiceTests extends BaseTest {

	private static final String INSTITUTE_CODE = "INS000";
	private static final String ACCESSION_NAME = "accessionName_data";
	private static final Integer ACQUISITION_SOURCE = null;
	private static final String ACQUISITION_DATE = "201503--";
	private static final String COUNTRY_OF_ORIGIN = "Zambia";
	private static final List<String> DUPL_SITE = Arrays.asList("SWE056", "INS000");

	private static final Integer SAMPLE_STATUS = 111;

	private static final Integer TAX_GENUS = 123232;

	private static final List<Integer> STORAGE_LIST = Arrays.asList(1, 2, 3, 4, 5);

	private static final Boolean IN_SVALBARD = true;
	private static final Boolean IN_TRAST = false;
	private static final Boolean AVALILABILITY = true;
	private static final Boolean HISTORIC = false;
	private static final Boolean MLS_STATUS = true;
	private static final Integer COUNTRY_OF_ORIGIN_ID = 1;

	@Test
	public void insertAccessionTest() {

		LOG.info("\n::: Start insertAccessionTest");

		Accession accession = new Accession();

		accession.setUuid(UUIDs.timeBased());
		accession.setAccessionName(ACCESSION_NAME);
		accession.setInstituteCode(INSTITUTE_CODE);
		accession.setAcquisitionSource(ACQUISITION_SOURCE);
		accession.setAcquisitionDate(ACQUISITION_DATE);
		accession.setCountryOfOrigin(COUNTRY_OF_ORIGIN);
		accession.setDuplSite(DUPL_SITE);
		accession.setStorage(STORAGE_LIST);
		accession.setSampleStatus(SAMPLE_STATUS);
		accession.setTaxGenus(TAX_GENUS);
		accession.setStorage(STORAGE_LIST);
		accession.setInSvalbard(IN_SVALBARD);
		accession.setInTrust(IN_TRAST);
		accession.setAvailability(AVALILABILITY);
		accession.setHistoric(HISTORIC);
		accession.setMlsStatus(MLS_STATUS);

		accessionService.insert(accession);

		Accession found_accession = accessionService.getAll().get(0);

		assertThat(found_accession.getInstituteCode(), is(INSTITUTE_CODE));
		assertThat(found_accession.getAccessionName(), is(ACCESSION_NAME));
		assertThat(found_accession.getAcquisitionSource(), is(ACQUISITION_SOURCE));
		assertThat(found_accession.getAcquisitionDate(), is(ACQUISITION_DATE));
		assertThat(found_accession.getCountryOfOrigin(), is(COUNTRY_OF_ORIGIN));
		assertThat(found_accession.getDuplSite(), is(DUPL_SITE));
		assertThat(found_accession.getStorage(), is(STORAGE_LIST));
		assertThat(found_accession.getSampleStatus(), is(SAMPLE_STATUS));
		assertThat(found_accession.getTaxGenus(), is(TAX_GENUS));
		assertThat(found_accession.getInSvalbard(), is(IN_SVALBARD));
		assertThat(found_accession.getInTrust(), is(IN_TRAST));
		assertThat(found_accession.getAvailability(), is(AVALILABILITY));
		assertThat(found_accession.isHistoric(), is(HISTORIC));
		assertThat(found_accession.getMlsStatus(), is(MLS_STATUS));

		LOG.info("InsertAccessionTest finished successfully!");
	}

	@Test
	public void deleteAccessionTest() {

		LOG.info("\n::: Start DeleteAccessionTest");

		Accession accession = new Accession();
		accession.setUuid(UUIDs.timeBased());

		accessionService.insert(accession);

		assertThat(accessionService.getAll().size(), is(1));

		accessionService.delete(accession);

		assertThat(accessionService.getAll().size(), is(0));

		LOG.info("DeleteAccessionTest finished successfully!");
	}

	@Test
	public void getAllTest() {
		LOG.info("\n::: Start getAllTest");

		assertThat(accessionService.getAll().size(), is(0));

		Accession accession_1 = new Accession();
		accession_1.setUuid(UUIDs.timeBased());

		Accession accession_2 = new Accession();
		accession_2.setUuid(UUIDs.timeBased());

		Accession accession_3 = new Accession();
		accession_3.setUuid(UUIDs.timeBased());

		accessionService.insert(accession_1);
		assertThat(accessionService.getAll().size(), is(1));

		accessionService.insert(accession_2);
		assertThat(accessionService.getAll().size(), is(2));

		accessionService.insert(accession_3);
		assertThat(accessionService.getAll().size(), is(3));

		LOG.info("getAllTest finished successfully!");
	}

	@Test
	public void getByIdTest() {
		LOG.info("\n::: Start getByIdTest");

		Accession accession = new Accession();

		UUID uuid = UUIDs.timeBased();

		accession.setUuid(uuid);
		accession.setAccessionName(ACCESSION_NAME);
		accession.setInstituteCode(INSTITUTE_CODE);
		accession.setAcquisitionSource(ACQUISITION_SOURCE);
		accession.setAcquisitionDate(ACQUISITION_DATE);
		accession.setCountryOfOrigin(COUNTRY_OF_ORIGIN);
		accession.setCountryOfOriginId(COUNTRY_OF_ORIGIN_ID);
		accession.setDuplSite(DUPL_SITE);
		accession.setStorage(STORAGE_LIST);
		accession.setSampleStatus(SAMPLE_STATUS);
		accession.setTaxGenus(TAX_GENUS);
		accession.setInSvalbard(IN_SVALBARD);
		accession.setInTrust(IN_TRAST);
		accession.setAvailability(AVALILABILITY);
		accession.setHistoric(HISTORIC);
		accession.setMlsStatus(MLS_STATUS);

		accessionService.insert(accession);

		Accession found_accession = accessionService.getById(uuid);

		assertThat(found_accession.getInstituteCode(), is(INSTITUTE_CODE));
		assertThat(found_accession.getAccessionName(), is(ACCESSION_NAME));
		assertThat(found_accession.getAcquisitionSource(), is(ACQUISITION_SOURCE));
		assertThat(found_accession.getAcquisitionDate(), is(ACQUISITION_DATE));
		assertThat(found_accession.getCountryOfOrigin(), is(COUNTRY_OF_ORIGIN));
		assertThat(found_accession.getCountryOfOriginId(), is(COUNTRY_OF_ORIGIN_ID));
		assertThat(found_accession.getDuplSite(), is(DUPL_SITE));
		assertThat(found_accession.getStorage(), is(STORAGE_LIST));
		assertThat(found_accession.getSampleStatus(), is(SAMPLE_STATUS));
		assertThat(found_accession.getTaxGenus(), is(TAX_GENUS));
		assertThat(found_accession.getInSvalbard(), is(IN_SVALBARD));
		assertThat(found_accession.getInTrust(), is(IN_TRAST));
		assertThat(found_accession.getAvailability(), is(AVALILABILITY));
		assertThat(found_accession.isHistoric(), is(HISTORIC));
		assertThat(found_accession.getMlsStatus(), is(MLS_STATUS));

		LOG.info(" getByIdTest finished successfully!");

	}

	@Test
	public void updateAccessionTest() {

		LOG.info("\n::: Start UpdateAccessionTest");
		/*
		 * create empty Accession and save it then retrieve and check then
		 * change and save again then retrieve and check changes
		 */
		Accession accession = new Accession();
		UUID uuid = UUIDs.timeBased();

		accession.setUuid(uuid);

		accessionService.insert(accession);

		Accession found_accession = accessionService.getById(uuid);

		assertThat(found_accession.getInstituteCode(), nullValue());
		assertThat(found_accession.getAccessionName(), nullValue());
		assertThat(found_accession.getAcquisitionSource(), nullValue());
		assertThat(found_accession.getAcquisitionDate(), nullValue());
		assertThat(found_accession.getCountryOfOrigin(), nullValue());
		assertThat(found_accession.getDuplSite(), nullValue());
		assertThat(found_accession.getStorage(), nullValue());
		assertThat(found_accession.getSampleStatus(), nullValue());
		assertThat(found_accession.getTaxGenus(), nullValue());
		assertThat(found_accession.getInSvalbard(), nullValue());
		assertThat(found_accession.getInTrust(), nullValue());
		assertThat(found_accession.getAvailability(), nullValue());
		assertThat(found_accession.isHistoric(), is(false));
		assertThat(found_accession.getMlsStatus(), nullValue());

		accession.setInstituteCode(INSTITUTE_CODE);
		accession.setAccessionName(ACCESSION_NAME);
		accession.setAcquisitionSource(ACQUISITION_SOURCE);
		accession.setAcquisitionDate(ACQUISITION_DATE);
		accession.setCountryOfOrigin(COUNTRY_OF_ORIGIN);
		accession.setDuplSite(DUPL_SITE);
		accession.setSampleStatus(SAMPLE_STATUS);
		accession.setTaxGenus(TAX_GENUS);
		accession.setStorage(STORAGE_LIST);
		accession.setInSvalbard(IN_SVALBARD);
		accession.setInTrust(IN_TRAST);
		accession.setAvailability(AVALILABILITY);
		accession.setHistoric(HISTORIC);
		accession.setMlsStatus(MLS_STATUS);

		accessionService.update(accession);

		found_accession = accessionService.getById(uuid);

		assertThat(found_accession.getInstituteCode(), is(INSTITUTE_CODE));
		assertThat(found_accession.getAccessionName(), is(ACCESSION_NAME));
		assertThat(found_accession.getAcquisitionSource(), is(ACQUISITION_SOURCE));
		assertThat(found_accession.getAcquisitionDate(), is(ACQUISITION_DATE));
		assertThat(found_accession.getCountryOfOrigin(), is(COUNTRY_OF_ORIGIN));
		assertThat(found_accession.getDuplSite(), is(DUPL_SITE));
		assertThat(found_accession.getStorage(), is(STORAGE_LIST));
		assertThat(found_accession.getSampleStatus(), is(SAMPLE_STATUS));
		assertThat(found_accession.getTaxGenus(), is(TAX_GENUS));
		assertThat(found_accession.getInSvalbard(), is(IN_SVALBARD));
		assertThat(found_accession.getInTrust(), is(IN_TRAST));
		assertThat(found_accession.getAvailability(), is(AVALILABILITY));
		assertThat(found_accession.isHistoric(), is(HISTORIC));
		assertThat(found_accession.getMlsStatus(), is(MLS_STATUS));

		LOG.info(" UpdateAccessionTest finished successfully!");
	}

	@Test
	public void getNonexistentEntryTest() {

		LOG.info("\n::: Start UpdateAccessionTest");

		UUID uuid = UUIDs.timeBased();

		Accession found_accession = accessionService.getById(uuid);

		assertThat(found_accession, nullValue());

		LOG.info(" UpdateAccessionTest finished successfully!");
	}

}
