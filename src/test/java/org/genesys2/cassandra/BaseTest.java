/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.cassandra;

import java.util.HashMap;

import org.genesys2.cassandra.config.CassandraConfigTest;
import org.genesys2.cassandra.entity.Accession;
import org.genesys2.cassandra.service.AccessionService;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cassandra.core.cql.CqlIdentifier;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.datastax.driver.core.exceptions.InvalidQueryException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CassandraConfigTest.class, loader = AnnotationConfigContextLoader.class)
public abstract class BaseTest {

	protected final Logger LOG = LoggerFactory.getLogger(this.getClass().getSimpleName());

	@Autowired
	protected CassandraAdminOperations adminTemplate;

	@Autowired
	protected AccessionService accessionService;

	@Before
	public void createTable() {
		LOG.info("Creating table accession");
		try {
			adminTemplate.createTable(false, CqlIdentifier.cqlId("accession"), Accession.class,
					new HashMap<String, Object>());
		} catch (InvalidQueryException e) {
			LOG.error("can't create table for " + this.getClass().getSimpleName() + " test", e);
		}
	}

	@After
	public void dropTable() {
		LOG.info("Dropping table accession");
		try {
			adminTemplate.dropTable(CqlIdentifier.cqlId("accession"));
		} catch (InvalidQueryException e) {
			LOG.error("can't drop table after" + this.getClass().getSimpleName() + " test", e);
		}
	}
}
