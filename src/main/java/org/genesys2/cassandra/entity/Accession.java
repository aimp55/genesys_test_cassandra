/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.cassandra.entity;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table
public class Accession {

	@PrimaryKey
	private UUID uuid;

	private String instituteCode;

	private String accessionName;

	private Integer acquisitionSource;

	private String acquisitionDate;

	private String countryOfOrigin;

	private Integer countryOfOriginId;

	private List<String> duplSite;

	private Integer sampleStatus;

	private Boolean inSvalbard;

	private Boolean inTrust;

	private Boolean availability;

	private boolean historic = false;

	private Boolean mlsStatus;

	private Integer taxGenus;

	private List<Integer> storage;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getInstituteCode() {
		return instituteCode;
	}

	public void setInstituteCode(String instituteCode) {
		this.instituteCode = instituteCode;
	}

	public String getAccessionName() {
		return accessionName;
	}

	public void setAccessionName(String accessionName) {
		this.accessionName = accessionName;
	}

	public Integer getAcquisitionSource() {
		return acquisitionSource;
	}

	public void setAcquisitionSource(Integer acquisitionSource) {
		this.acquisitionSource = acquisitionSource;
	}

	public String getAcquisitionDate() {
		return acquisitionDate;
	}

	public void setAcquisitionDate(String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public List<String> getDuplSite() {
		return duplSite;
	}

	public void setDuplSite(List<String> duplSite) {
		this.duplSite = duplSite;
	}

	public Integer getSampleStatus() {
		return sampleStatus;
	}

	public void setSampleStatus(Integer sampleStatus) {
		this.sampleStatus = sampleStatus;
	}

	public Boolean getInSvalbard() {
		return inSvalbard;
	}

	public void setInSvalbard(Boolean inSvalbard) {
		this.inSvalbard = inSvalbard;
	}

	public Boolean getInTrust() {
		return inTrust;
	}

	public void setInTrust(Boolean inTrust) {
		this.inTrust = inTrust;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	public boolean isHistoric() {
		return historic;
	}

	public void setHistoric(boolean historic) {
		this.historic = historic;
	}

	public Boolean getMlsStatus() {
		return mlsStatus;
	}

	public void setMlsStatus(Boolean mlsStatus) {
		this.mlsStatus = mlsStatus;
	}

	public Integer getTaxGenus() {
		return taxGenus;
	}

	public void setTaxGenus(Integer taxGenus) {
		this.taxGenus = taxGenus;
	}

	public List<Integer> getStorage() {
		return storage;
	}

	public void setStorage(List<Integer> storage) {
		this.storage = storage;
	}
	
	public Integer getCountryOfOriginId() {
		return countryOfOriginId;
	}
	
	public void setCountryOfOriginId(Integer countryOfOriginId) {
		this.countryOfOriginId = countryOfOriginId;
	}
}